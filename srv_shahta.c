#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>

int main(void)
{
	int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	int stat = 0;
	char OutBuff[1024] = {0};

	printf("GovnoSoft Shahta Server Enterprise Pro!\r\n");
	printf("Built: %s %s\r\n", __DATE__, __TIME__);

	printf("Сервер запущен!\r\n");

	struct sockaddr_in srv_addr;
	srv_addr.sin_addr.s_addr = INADDR_ANY;
	srv_addr.sin_port = htons(6666);
	srv_addr.sin_family = AF_INET;	

	stat = bind(sock_fd, (struct sockaddr *)&srv_addr, sizeof(srv_addr));
	if(stat == -1)
	{
		printf("\x1b[31mBIND\x1b[0m error!\r\n");
		exit(-1);
	};
	listen(sock_fd, 2048);
	printf("Сокет создан и слушает на порту 6666\r\n");
	while(1)
	{
		int conn_sock = accept(sock_fd, NULL, NULL);
		printf("Клиент подключен!\r\n");
		sprintf(OutBuff, "Завтрак шахтерский!\r\n");
		write(conn_sock, OutBuff, strlen(OutBuff));
		for(int i=0;i<10;i++)
		{
			sprintf(OutBuff, "Шахта №%d\r\n", i);
			write(conn_sock, OutBuff, strlen(OutBuff));
		};
		close(conn_sock);
		printf("Сеанс завершен!\r\n");		
	};
};
